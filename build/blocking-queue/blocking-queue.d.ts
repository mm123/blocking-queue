import { Observable } from 'rxjs';
export declare class BlockingQueue<T> {
    private readonly elements;
    private readonly current$;
    private blocked;
    constructor(elements?: T[]);
    get element(): Observable<T>;
    get size(): number;
    next(): void;
    complete(): void;
    push(element: T): void;
    private emitNextElement;
}
