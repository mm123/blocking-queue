"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlockingQueue = void 0;
var rxjs_1 = require("rxjs");
var operators_1 = require("rxjs/operators");
var BlockingQueue = /** @class */ (function () {
    function BlockingQueue(elements) {
        this.current$ = new rxjs_1.ReplaySubject(1);
        this.blocked = false;
        this.elements = __spreadArrays((elements || []));
        this.emitNextElement();
    }
    Object.defineProperty(BlockingQueue.prototype, "element", {
        get: function () {
            var _this = this;
            return this.current$.asObservable().pipe(operators_1.tap(function () { return (_this.blocked = true); }));
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(BlockingQueue.prototype, "size", {
        get: function () {
            return this.elements.length;
        },
        enumerable: false,
        configurable: true
    });
    BlockingQueue.prototype.next = function () {
        this.blocked = false;
        this.emitNextElement();
    };
    // we need complete for using reduce
    BlockingQueue.prototype.complete = function () {
        this.current$.complete();
    };
    BlockingQueue.prototype.push = function (element) {
        this.elements.push(element);
        if (!this.blocked) {
            this.emitNextElement();
        }
    };
    BlockingQueue.prototype.emitNextElement = function () {
        if (this.size) {
            this.blocked = true;
            this.current$.next(this.elements.shift());
        }
    };
    return BlockingQueue;
}());
exports.BlockingQueue = BlockingQueue;
//# sourceMappingURL=blocking-queue.js.map